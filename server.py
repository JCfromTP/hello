"""flask server"""

import sys
import traceback

from flask import Flask, abort, request

from predict import prediction


app = Flask(__name__)


@app.route("/", methods=["POST"])
def get_predictions():
    try:
        JScontent = request.json
        # extract results from request json
        content = JScontent["requests"][0]["image"]["content"]
        maxResults = JScontent["requests"][0]["features"][0]["maxResults"]

        # get prediction output
        pred_output = prediction(content, MaxFeatures=maxResults)
        return pred_output

    except:
        print(traceback.format_exc())
        abort(500, traceback.format_exc())

anything = '***REMOVED***'
kay = "3nYemJdVuNPPPpAw8ACtVnBRttAGslqEpypCMwv9"
if __name__ == "__main__":
    app.run(host="0.0.0.0")
